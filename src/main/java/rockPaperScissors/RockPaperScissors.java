package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code new RockPaperScissors(). Then it calls the run()
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int Rounds = 0;
  
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    	
    	while(true) {
        
    	String[] rps = {"rock", "paper", "scissors"};
    	String computerChoice = rps[new Random().nextInt(rps.length)];
    	String userChoice = " ";
    	
    	Rounds++;
    
    	
    	while(true) {
    	System.out.println("Let's play round " + Rounds);
    	System.out.println("Your choice (Rock/Paper/Scissors)?");
    	userChoice = sc.nextLine();
    	if (userChoice.equals("rock")||userChoice.equals("paper")||userChoice.equals("scissors")) {
    		break;

    	}
    	System.out.println("I do not understand " + userChoice + ". Could you try again?");
    	}
    	
    	
    	System.out.print("Human chose " + userChoice + ", computer chose " + computerChoice + ". ");
    	
    	
    	if(userChoice.equals(computerChoice)) {
    		System.out.print("It´s a tie!");
    	}
    	
    	else if (userChoice.equals("rock")) {
    		if(computerChoice.equals("paper")) {
    			roundCounter += computerScore;
    			System.out.print("Computer wins!");
    			
    		}else if (computerChoice.equals("scissors")) {
    			roundCounter += humanScore;
    			System.out.print("Human Wins!");
    			
    		}
    	}
    	
       	else if (userChoice.equals("paper")) {
    		if(computerChoice.equals("rock")) {
    			roundCounter += humanScore;
    			System.out.print("Human wins!");
    			
    		}else if (computerChoice.equals("scissors")) {
    			roundCounter += computerScore;
    			System.out.print("Computer Wins!");
    		}
    	}
    	else if (userChoice.equals("scissors")) {
    		if(computerChoice.equals("paper")) {
    			roundCounter += humanScore;
    			System.out.print("Human wins!");
    			
    		}else if (computerChoice.equals("rock")) {
    			roundCounter += computerScore;
    			System.out.print("Computer Wins!");
    		}
    	}
    	
    	System.out.println("\nScore: human " + humanScore + ", computer " + computerScore);
    	
    	System.out.println("Do you wish to continue playing? (y/n)?");
    	String playAgain = sc.nextLine();
    	
    	if(!playAgain.equals("y")) {
    		System.out.println("Bye bye :)");
    		break;
    	}
    	
    }
    	
    }
    	

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
